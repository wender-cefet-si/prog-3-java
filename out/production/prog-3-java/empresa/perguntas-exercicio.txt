

Página 89

7) Suponha que você agora precisa criar uma classe ContaInvestimento, cujo
método atualiza é complicadíssimo. Nesse caso você precisaria alterar as classes
Banco e AtualizadorDeContas?
8) Existe Polimorfismo sem Herança?
9) É realmente necessário colocar os atributos como protected em herança? Preciso
realmente afrouxar o encapsulamento do atributo por causa da herança? Como
posso fazer para deixar os atributos como private na superclasse e as subclasses
conseguirem de alguma forma trabalhar com eles?