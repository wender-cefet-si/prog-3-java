package empresa;

/**
 * Created by wender on 4/2/16.
 */
public class AtualizadorDeContas {
    private double saldoTotal = 0;
    private double selic;

    public double getSaldoTotal() {
        return saldoTotal;
    }


    private void atualizaSaldoTotal(double valor) {
        this.saldoTotal += valor;
    }

    public AtualizadorDeContas(double selic) {
        this.selic = selic;
    }

    public void roda(Conta conta) {
        double saldo = conta.getSaldo();
        System.out.println("Saldo inicial: " + saldo);
        conta.atualizar(this.selic);
        saldo = conta.getSaldo();
        System.out.println("Saldo pós atualizar: " + saldo);
        System.out.println("");
        this.atualizaSaldoTotal(saldo);
    }
}
