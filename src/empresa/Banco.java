package empresa;

import java.util.Arrays;

/**
 * Created by wender on 4/2/16.
 */
public class Banco {
	private Conta[] contas;
	private int posicaoVazia = 0;

	public Conta[] getContas() {
		return contas;
	}

	public Conta getContaPelaPosicao(int posicao) {
		return contas[posicao];
	}

	public int getTotalDeContas() {
		return this.contas.length;
	}

	public void setContas(Conta[] contas) {
		this.contas = contas;
	}

	public Banco(int numeroDeContas) {
		this.contas = new Conta[numeroDeContas];
	}

	private void redimensiona() {
		this.contas = Arrays.copyOf(this.contas, this.contas.length + 1);
	}

	public void adicionaConta(Conta conta) {
		if (!(this.contas.length < this.posicaoVazia))
			this.redimensiona();
		contas[this.posicaoVazia] = conta;
		this.posicaoVazia++;
	}
}
