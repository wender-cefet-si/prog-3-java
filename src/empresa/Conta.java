package empresa;

public class Conta {
    private double saldo;
    private double limite;
    private Cliente titular = new Cliente();
    private int numero;
    private static int totalDeContas;
    public static final int MULTIPLICADOR_TAXA = 1;

    public Conta() {
        this.limite = 100;
        Conta.totalDeContas = Conta.totalDeContas + 1;
    }

    public static int getTotalDeContas() {
        return Conta.totalDeContas;
    }

    public double getSaldo() {
        return saldo;
    }

    public void exibirSaldo() {
        System.out.println("O saldo atual é: " + (this.saldo + this.limite));
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }

    public double getLimite() {
        return limite;
    }

    public void setLimite(double limite) {
        this.limite = limite;
    }

    public Cliente getTitular() {
        return titular;
    }

    public void setTitular(Cliente titular) {
        this.titular = titular;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public static void setTotalDeContas(int totalDeContas) {
        Conta.totalDeContas = totalDeContas;
    }

    public boolean depositar(double valor) {
        if (valor > 0) {
            this.saldo += valor;
            return true;
        } else {
            return false;
        }
    }

    public boolean saca(double valor) {
        if (valor > 0 && valor <= (this.saldo + this.limite)) {
            this.saldo -= valor;
            return true;
        } else {
            return false;
        }
    }

    public boolean transferePara(Conta contaDestino, double valor) {
        Conta contaOrigem = this;
        if (contaOrigem.saca(valor)) {
            return contaDestino.depositar(valor);
        }
        return false;
    }

    public void atualizar(double taxa) {
        this.saldo -= taxa;
    }
}
