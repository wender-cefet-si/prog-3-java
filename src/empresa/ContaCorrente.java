package empresa;

public class ContaCorrente extends Conta {
    public static final int MULTIPLICADOR_TAXA = 2;
    public static final double TAXA_BANCARIA = 0.1;

    public void atualizar(double taxa) {
        this.setSaldo(this.getSaldo() - (taxa * MULTIPLICADOR_TAXA));
    }

    public boolean depositar(double valor) {
        if (valor > 0) {
            this.setSaldo(this.getSaldo() + valor);
            this.setSaldo(this.getSaldo() - TAXA_BANCARIA);
            return true;
        }
        return false;
    }
}