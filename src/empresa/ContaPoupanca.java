package empresa;

public class ContaPoupanca extends Conta {
    public static final int MULTIPLICADOR_TAXA = 3;

    public void atualizar(double taxa) {
        this.setSaldo(this.getSaldo() - (taxa * MULTIPLICADOR_TAXA));
    }
}