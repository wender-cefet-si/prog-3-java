package empresa;

public class ControleDeBonificacoes {
    private double totalDeBonificacoes;

	public double getTotalDeBonificacoes() {
		return totalDeBonificacoes;
	}

	public void setTotalDeBonificacoes(double totalDeBonificacoes) {
		this.totalDeBonificacoes = totalDeBonificacoes;
	}

    public void acumulaBonificacoes(Funcionario funcionario) {
        setTotalDeBonificacoes(getTotalDeBonificacoes() + funcionario.getBonificacao());
    }

	public void registra(Funcionario funcionario) {
        this.totalDeBonificacoes += funcionario.getBonificacao();
    }
}