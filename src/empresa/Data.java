package empresa;

public class Data {
    private String dia, mes, ano;

    // get's set's
    public Data() {
    }

    public Data(String dia, String mes, String ano) {
        this.dia = dia;
        this.mes = mes;
        this.ano = ano;
    }

    public String dataToString() {
        return this.dia + "/" + this.mes + "/" + this.ano;
    }

    public String toString() {
        return this.dataToString();
    }
}
