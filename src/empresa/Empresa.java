package empresa;
//import java.util.Arrays;

public class Empresa {
    private String nome, cpnj;
    private Funcionario[] funcionarios;
    private int posicaoLivre;

    public Empresa(String nome, String cpnj) {
        this.nome = nome;
        this.cpnj = cpnj;
        //funcionarios = new Funcionario[1];
    }

    public Empresa(String nome, String cpnj, int quantidadeFuncionarios) {
        this.nome = nome;
        this.cpnj = cpnj;
        funcionarios = new Funcionario[quantidadeFuncionarios];
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpnj() {
        return cpnj;
    }

    public void setCpnj(String cpnj) {
        this.cpnj = cpnj;
    }

    public Funcionario[] getEmpregados() {
        return funcionarios;
    }

    public Funcionario getFuncionario(int posicao) {
        return this.funcionarios[posicao];
    }

    public void setFuncionarios(Funcionario[] empregados) {
        this.funcionarios = empregados;
    }

    public int getPosicaoLivre() {
        return posicaoLivre;
    }

    public void setPosicaoLivre(int posicaoLivre) {
        this.posicaoLivre = posicaoLivre;
    }

    public boolean adiciona(Funcionario funcionario) {
        try {
            if (this.funcionarios[this.posicaoLivre] instanceof Funcionario) {
                return false;
            } else {
                this.funcionarios[this.posicaoLivre] = funcionario;
                this.posicaoLivre++;
                return true;
            }
        } catch (Exception e) {
            return false;
        }
    }

    public void aumentarNumeroEmpregados(int acrecimo) {
        int tamanhoFinal = (posicaoLivre - 1) + acrecimo;
        Funcionario[] newEmpregados = new Funcionario[tamanhoFinal];
        //newEmpregados = Arrays.copyOf(this.empregados, this.empregados.length);
        this.funcionarios = newEmpregados;
    }

    public void mostraEmpregados() {
        for (Funcionario empregado : funcionarios) {
            if (empregado == null) {
                break;
            } else {
                empregado.imprimeDetalhes();
            }
        }
    }

    public boolean funcionarioExiste(Funcionario funcionario) {
        for (Funcionario empregado : funcionarios) {
            if (empregado == null) {
                break;
            } else {
                if (funcionario.getId() == empregado.getId())
                    return true;
            }
        }
        return false;
    }

    public void alocar() {
        Funcionario[] novoArray = new Funcionario[this.funcionarios.length + 1];
        for (int i = 0; i < this.funcionarios.length; i++) {
            novoArray[i] = funcionarios[i];
        }
        this.funcionarios = novoArray;
    }
}
