package empresa;

public class Funcionario {
    private boolean ativo;
    private String cpf;
    private Departamento departamento;
    private int id;
    private String nome;
    private double salario;
    private Data dataNascimento;

    public Funcionario(String nome) {
        this.nome = nome;
    }

    public Funcionario(String nome, double salario) {
        this.nome = nome;
        this.salario = salario;
    }

    public Funcionario(String nome, double salario, Departamento departamento) {
        this.departamento = departamento;
        this.nome = nome;
        this.salario = salario;
        this.dataNascimento = new Data();
        this.ativo = true;
    }

    public Funcionario(String nome, String cpf, double salario, Departamento departamento) {
        this.setCpf(cpf);
        this.departamento = departamento;
        this.nome = nome;
        this.salario = salario;
        this.dataNascimento = new Data();
        this.ativo = true;
    }

    public boolean setCpf(String cpf) {
        if (this.cpfValido(cpf)) {
            this.cpf = cpf;
            return true;
        } else
            return false;
    }

    public String getCpf() {
        return this.cpf;
    }

    public boolean cpfValido(String cpf) {
        if (cpf.length() == 11)
            return true;
        else
            return false;
    }

    public boolean isAtivo() {
        return this.ativo;
    }

    public int getId() {
        return this.id;
    }

    public String getNome() {
        return this.nome;
    }

    public double getSalario() {
        return this.salario;
    }

    public Departamento getDepartamento() {
        return this.departamento;
    }

    public Data getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(Data dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setSalario(double salario) {
        this.salario = salario;
    }

    public void aumentarSalario(double percentual) {
        this.salario += ((percentual * salario) / 100);
    }

    public void demite() {
        this.ativo = false;
    }

    public String funcionarioToString() {
        String texto = "Nome: " + this.getNome() + System.lineSeparator() +
                "Salário: " + this.getSalario() + System.lineSeparator() +
                "Departamento: " + this.getDepartamento().getNome() + System.lineSeparator() +
                "Está na empresa: ";
        texto += (this.isAtivo()) ? "Sim" : "Não";
        return texto;
    }

    public void imprimeDetalhes() {
        System.out.println(this.funcionarioToString());
    }

    public double getBonificacao() {
        return this.salario * 0.10;
    }

}
