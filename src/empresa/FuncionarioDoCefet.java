package empresa;

/**
 * Created by wender on 4/2/16.
 */
public class FuncionarioDoCefet extends Funcionario {
    private int matriculaSiape;

    public int getMatriculaSiape() {
        return matriculaSiape;
    }

    public void setMatriculaSiape(int matriculaSiape) {
        this.matriculaSiape = matriculaSiape;
    }

    public FuncionarioDoCefet(String nome) {
        super(nome);
    }

    public FuncionarioDoCefet(String nome, double salario) {
        super(nome, salario);
    }

    public double getGastos() {
        return this.getSalario();
    }

    public String getInfo() {
        return "Nome: " + this.getNome() + ", Salário: " + this.getSalario();
    }
}
