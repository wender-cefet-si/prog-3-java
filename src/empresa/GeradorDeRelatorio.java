package empresa;

/**
 * Created by wender on 4/2/16.
 */
public class GeradorDeRelatorio {
    public FuncionarioDoCefet[] empregados;

    public GeradorDeRelatorio(FuncionarioDoCefet[] arrayDeFuncionarios) {
        this.empregados = arrayDeFuncionarios;
    }

    public void imprime() {
        for (FuncionarioDoCefet funcionarioDoCefet : empregados) {
            System.out.println(funcionarioDoCefet.getInfo());
            System.out.println("Gastos: " + funcionarioDoCefet.getGastos());
            System.out.println("###########################################");
        }
    }
}
