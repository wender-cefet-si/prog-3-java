package empresa;

public class Gerente extends Funcionario {
    private String senha;
//    private int numeroDeFuncionariosGerenciados;

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public Gerente(String nome) {
        super(nome);
    }

    public double getBonificacao() {
        return super.getBonificacao() + 1000;
    }

    public Gerente(String nome, double salario, Departamento departamento) {
        super(nome, salario, departamento);
    }

    public boolean login(String senha) {
        return (this.senha == senha);
    }
}
