package empresa;

/**
 * Created by wender on 4/2/16.
 */
public class ProfessorDoCefet extends FuncionarioDoCefet {
    private double auxilioAlimentacao = 400.00;

    public ProfessorDoCefet(String nome, double salario) {
        super(nome, salario);
    }

    @Override
    public double getGastos() {
        return this.auxilioAlimentacao + super.getGastos();
    }

    @Override
    public String getInfo() {
        return super.getInfo() + " ( " + this.auxilioAlimentacao + " referente ao auxilio alimentacao )";
    }
}
