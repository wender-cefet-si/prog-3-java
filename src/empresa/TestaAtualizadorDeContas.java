package empresa;

/**
 * Created by wender on 4/2/16.
 */
public class TestaAtualizadorDeContas {
    public static void main(String[] args) {
        Conta conta = new Conta();
        Conta contaCorrente = new ContaCorrente();
        Conta contaPoupanca = new ContaPoupanca();

        conta.depositar(1000);
        contaCorrente.depositar(1000);
        contaPoupanca.depositar(1000);

        AtualizadorDeContas atualizadorDeContas = new AtualizadorDeContas(0.01);
        atualizadorDeContas.roda(conta);
        atualizadorDeContas.roda(contaCorrente);
        atualizadorDeContas.roda(contaPoupanca);

        System.out.println("Saldo Total: " + atualizadorDeContas.getSaldoTotal());
    }
}
