package empresa;

/**
 * Created by wender on 4/2/16.
 */
public class TestaBanco {
    public static void main(String[] args) {
        Conta contaComum = new Conta();
        Conta contaCorrente = new ContaCorrente();
        Conta contaPoupanca = new ContaPoupanca();

        contaComum.depositar(1000);
        contaCorrente.depositar(1000);
        contaPoupanca.depositar(1000);

        Banco banco = new Banco(3);
        banco.adicionaConta(contaComum);
        banco.adicionaConta(contaCorrente);
        banco.adicionaConta(contaPoupanca);

        AtualizadorDeContas atualizadorDeContas = new AtualizadorDeContas(0.01);
        for (Conta conta : banco.getContas()) {
            atualizadorDeContas.roda(conta);
        }

        System.out.println("Saldo Total: " + atualizadorDeContas.getSaldoTotal());
    }
}
