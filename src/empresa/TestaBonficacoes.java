package empresa;

/**
 * Created by wender on 4/2/16.
 */
public class TestaBonficacoes {
    public static void main(String[] args) {
        ControleDeBonificacoes controle = new ControleDeBonificacoes();
        Gerente gerente = new Gerente("Junior");
        gerente.setSalario(5000);
        controle.registra(gerente);

        Funcionario funcionario = new Funcionario("Manuel");
        funcionario.setSalario(2000);
        controle.registra(funcionario);

        System.out.println(controle.getTotalDeBonificacoes());
    }
}
