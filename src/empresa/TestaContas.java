package empresa;

/**
 * Created by wender on 4/2/16.
 */
public class TestaContas {
    public static void main(String[] args) {
//        Conta conta = new Conta();
//        ContaCorrente contaCorrente = new ContaCorrente();
//        ContaPoupanca contaPoupanca = new ContaPoupanca();

        Conta conta = new Conta();
        Conta contaCorrente = new ContaCorrente();
        Conta contaPoupanca = new ContaPoupanca();

        conta.depositar(1000);
        contaCorrente.depositar(1000);
        contaPoupanca.depositar(1000);

        System.out.println("Saldo das contas depois de depositar:");
        System.out.println("Conta comum:" + conta.getSaldo());
        System.out.println("Conta corrente:" + contaCorrente.getSaldo());
        System.out.println("Conta poupança:" + contaPoupanca.getSaldo());

        conta.atualizar(0.01);
        contaPoupanca.atualizar(0.01);
        contaCorrente.atualizar(0.01);

//        conta.depositar(0.01);
//        contaPoupanca.depositar(0.01);
//        contaCorrente.depositar(0.01);

        System.out.println("");
        System.out.println("Saldo das contas depois de atualizar:");
        System.out.println("Conta comum:" + conta.getSaldo());
        System.out.println("Conta corrente:" + contaCorrente.getSaldo());
        System.out.println("Conta poupança:" + contaPoupanca.getSaldo());
    }
}
