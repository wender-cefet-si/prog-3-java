package empresa;

/**
 * Created by wender on 4/2/16.
 */
public class TestaGeradorDeRelatorios {
    public static void main(String[] args) {
        ProfessorDoCefet rodrigo = new ProfessorDoCefet("Rodrigo", 8000);
        ProfessorDoCefet rafael = new ProfessorDoCefet("Rafael", 3000);
        ProfessorDoCefet joao = new ProfessorDoCefet("João", 2000);
        ProfessorDoCefet maria = new ProfessorDoCefet("Maria", 2500);
        FuncionarioDoCefet edu = new FuncionarioDoCefet("Edu", 1000);

        FuncionarioDoCefet[] funcionarios = new FuncionarioDoCefet[5];
        funcionarios[0] = rodrigo;
        funcionarios[1] = rafael;
        funcionarios[2] = joao;
        funcionarios[3] = maria;
        funcionarios[4] = edu;

        GeradorDeRelatorio gerador = new GeradorDeRelatorio(funcionarios);
        gerador.imprime();
    }
}
