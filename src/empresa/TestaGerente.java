package empresa;

public class TestaGerente {
    public static void main(String[] args) {
        Departamento ti = new Departamento("TI");
        Gerente gerente = new Gerente("Wender", 10000, ti);
        gerente.setSenha("abcd1234");

        if (gerente.login("abcd1234"))
            System.out.println("Login realizado com sucesso!");
        else
            System.out.println("Senha inválida");
    }
}
