package interfaces;

/**
 * Created by wender on 4/2/16.
 */
public interface AreaCalculavel {
    double calculaArea();
}
