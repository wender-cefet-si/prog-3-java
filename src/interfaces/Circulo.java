package interfaces;

/**
 * Created by wender on 4/2/16.
 */
public class Circulo implements AreaCalculavel {
    private double raio;

    public Circulo(double raio) {
        this.raio = raio;
    }

    @Override
    public double calculaArea() {
        return Math.PI * this.raio;
    }
}
