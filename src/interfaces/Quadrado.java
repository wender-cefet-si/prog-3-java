package interfaces;

/**
 * Created by wender on 4/2/16.
 */
public class Quadrado implements AreaCalculavel {
    private int lado;

    public Quadrado(int lado) {
        this.lado = lado;
    }

    public int getLado() {
        return lado;
    }

    @Override
    public double calculaArea() {
        return this.lado * this.lado;
    }
}
