package interfaces;

/**
 * Created by wender on 4/2/16.
 */
public class Retangulo implements AreaCalculavel {
    private int largura, altura;

    public Retangulo(int largura, int altura) {
        this.largura = largura;
        this.altura = altura;
    }

    public int getAltura() {
        return altura;
    }

    public int getLargura() {
        return largura;
    }

    @Override
    public double calculaArea() {
        return this.altura * this.largura;
    }
}
