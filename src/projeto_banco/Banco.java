package projeto_banco;

import java.util.Arrays;

/**
 * Created by wender on 4/2/16.
 */
public class Banco {
    private Conta[] contas;
    private int posicaoLivre = 0;

    public Banco(int numeroDeContas) {
        this.contas = new Conta[numeroDeContas];
    }

    public void adiciona(Conta c) {
        if (!(this.contas.length < posicaoLivre))
            this.redimensiona();
        contas[this.posicaoLivre] = c;
        this.posicaoLivre++;
    }

    private void redimensiona() {
        this.contas = Arrays.copyOf(this.contas, this.contas.length + 1);
    }

    public Conta pegaConta(int posicao) {
        if (contas[posicao] != null)
            return contas[posicao];
        return null;
    }

    public int getTotalDeContas() {
        return this.contas.length;
    }

    public Conta[] getContas() {
        return contas;
    }

    public void setContas(Conta[] contas) {
        this.contas = contas;
    }
}
