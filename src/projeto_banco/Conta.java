package projeto_banco;

/**
 * Created by wender on 4/2/16.
 */
public abstract class Conta {
    protected double saldo;
    protected double limite;
    protected Cliente cliente = new Cliente();

    public boolean deposita(double valor) {
        if (valor > 0) {
            this.saldo += valor;
            return true;
        }
        return false;
    }

    public void saca(double valor) throws SaldoInsuficienteException {
        if (valor > 0 && valor <= (this.saldo + this.limite)) {
            this.saldo -= valor;
        } else {
            throw new SaldoInsuficienteException("Saldo insuficiente");
        }
    }

    public void transferePara(Conta contaDestino, double valor) {
        try {
            this.saca(valor);
            contaDestino.deposita(valor);
        } catch (SaldoInsuficienteException e){
            System.out.println(e);
        }
    }

    public abstract void atualiza(double taxa);

    //Métodos get e set....
    public double getLimite() {
        return limite;
    }

    public void setLimite(double limite) {
        this.limite = limite;
    }

    public double getSaldo() {
        return saldo;
    }

    public Conta() {
        this.limite = 100;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public void exibirSaldo() {
        System.out.println("O saldo atual é: " + (saldo + limite));
    }
}