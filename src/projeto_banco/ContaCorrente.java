package projeto_banco;

/**
 * Created by wender on 4/2/16.
 */
public class ContaCorrente extends Conta implements Tributavel {
    public static final double TRIBUTOS = 0.01;

    @Override
    public void atualiza(double taxa) {
        this.saldo -= (2 * taxa);
    }

    @Override
    public boolean deposita(double valor) {
        if (valor > 0) {
            this.saldo += valor;
            this.saldo -= 0.10;
            return true;
        }
        return false;
    }

    @Override
    public double calculaTributos() {
        return this.saldo * TRIBUTOS;
    }
}