package projeto_banco;

/**
 * Created by wender on 4/2/16.
 */
public class ContaPoupanca extends Conta {
    @Override
    public void atualiza(double taxa) {
        this.saldo -= (3 * taxa);
    }
}