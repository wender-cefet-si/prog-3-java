package projeto_banco;

/**
 * Created by wender on 4/2/16.
 */
public class GerenciadorDeImpostoDeRenda {
    private double total;

    public double getTotal() {
        return total;
    }

    public void adicionaTributavel(Tributavel tributavel) {
        System.out.println("Adicionado tributavel" + tributavel);
        this.total += tributavel.calculaTributos();
    }
}
