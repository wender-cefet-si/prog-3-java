package projeto_banco;

/**
 * Created by Wender on 11/04/2016.
 */
public class SaldoInsuficienteException extends Exception {
    public SaldoInsuficienteException(String mensagem) {
        super(mensagem);
    }
}
