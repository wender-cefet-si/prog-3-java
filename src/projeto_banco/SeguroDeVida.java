package projeto_banco;

/**
 * Created by wender on 4/2/16.
 */
public class SeguroDeVida implements Tributavel {
    public static final double TRIBUTOS = 50;

    @Override
    public double calculaTributos() {
        return TRIBUTOS;
    }
}
