package projeto_banco;

/**
 * Created by wender on 4/2/16.
 */
public class TestaContas {
    public static void main(String[] args) {
        // Conta c = new Conta(); // Não instancia mais pois virou abstrata
        ContaCorrente cc = new ContaCorrente();
        ContaPoupanca cp = new ContaPoupanca();
//        c.deposita(1000);
        cc.deposita(1000);
        cp.deposita(1000);
        System.out.println("Saldo das contas antes de atualizar:");
//        System.out.println("Conta c: " + c.getSaldo());
        System.out.println("ContaCorrente cc: " + cc.getSaldo());
        System.out.println("ContaPoupanca cp: " + cp.getSaldo());
//        c.atualiza(0.01);
        cc.atualiza(0.01);
        cp.atualiza(0.01);
        //System.err faz imprimir em vermelho
        System.err.println("Saldo das contas depois de atualizar:");
//        System.err.println("Conta c: " + c.getSaldo());
        System.err.println("ContaCorrente cc: " + cc.getSaldo());
        System.err.println("ContaPoupanca cp: " + cp.getSaldo());
    }
}
