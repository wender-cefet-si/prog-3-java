package projeto_banco;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

/**
 * Created by Wender on 11/04/2016.
 */
public class TestaException {
    public static void main(String[] args) {
//        Conta conta;
        Conta conta = new ContaCorrente();
//        int dividor = 0;
        int dividor = 2;
        String arquivo = "arq.txt";

        try {
            abreArquivo(arquivo);
            int x = 10 / dividor;
            mostraSaldo(conta);
        } catch (FileNotFoundException e) {
            System.out.println("Arquivo não encontrado." + e.getMessage());
        } catch (ArithmeticException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            System.out.println("Referencia nula." + e.getMessage());
        }
    }

    public static void abreArquivo(String arquivo) throws FileNotFoundException {
        new FileInputStream(arquivo);
    }

    public static void mostraSaldo(Conta conta) {
        System.out.println(conta.getSaldo());
    }

}
