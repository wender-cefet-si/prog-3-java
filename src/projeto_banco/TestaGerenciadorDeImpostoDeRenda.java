package projeto_banco;

/**
 * Created by wender on 4/2/16.
 */
public class TestaGerenciadorDeImpostoDeRenda {
    public static void main(String[] args) {
        GerenciadorDeImpostoDeRenda gerenciadorDeImpostoDeRenda = new GerenciadorDeImpostoDeRenda();

        SeguroDeVida seguroDeVida = new SeguroDeVida();
        gerenciadorDeImpostoDeRenda.adicionaTributavel(seguroDeVida);

        ContaCorrente contaCorrente = new ContaCorrente();
        contaCorrente.deposita(1000);
        gerenciadorDeImpostoDeRenda.adicionaTributavel(contaCorrente);

        System.out.printf("O saldo é: %.2f", gerenciadorDeImpostoDeRenda.getTotal());
    }
}
