package projeto_banco;

/**
 * Created by wender on 4/2/16.
 */
public class TestaTributavel {
    public static void main(String[] args) {
        ContaCorrente contaCorrente = new ContaCorrente();
        contaCorrente.deposita(100);
        System.out.println(contaCorrente.calculaTributos());

        SeguroDeVida seguroDeVida = new SeguroDeVida();
        System.out.println(seguroDeVida.calculaTributos());

        Tributavel tributavel = contaCorrente;
        System.out.println(tributavel.calculaTributos());
    }
}
