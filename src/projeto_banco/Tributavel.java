package projeto_banco;

/**
 * Created by wender on 4/2/16.
 */
public interface Tributavel {
    public final double MINIMO = 622;
    double calculaTributos();
}
