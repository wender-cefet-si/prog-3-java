package projeto_collection;

/**
 * Created by wender on 5/29/16.
 */
public class ContaPoupanca extends empresa.ContaPoupanca implements Comparable<ContaPoupanca> {
    // Crescente
//    @Override
//    public int compareTo(ContaPoupanca outraConta) {
//        return this.getNumero()-outraConta.getNumero();
//    }
    // Decrescente
    @Override
    public int compareTo(ContaPoupanca outraConta) {
        return outraConta.getNumero()-this.getNumero();
    }
}
