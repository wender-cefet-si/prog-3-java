package projeto_collection;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;

/**
 * Created by wender on 5/29/16.
 */
public class TestaDesempenho {
    public static void main(String[] args) {
        // CRITÉRIO: COMPARAR
        // ARRAY LIST VS HASHSET
        // HASHSET WIN

        System.out.println("Iniciando...");
        long inicio=System.currentTimeMillis();
//        Collection<Integer> numeros = new ArrayList<Integer>();
        Collection<Integer> numeros = new HashSet<Integer>();
        int total=300000;
        for (int i = 0; i < total; i++) {
            numeros.add(i);
        }
        for (int i = 0; i < total; i++) {
            numeros.contains(i);
        }
        long fim=System.currentTimeMillis();
        long tempo=fim-inicio;
        System.out.println("Tempo gasto: "+tempo);
    }
}
