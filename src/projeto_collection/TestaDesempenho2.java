package projeto_collection;

import java.util.*;

/**
 * Created by wender on 5/29/16.
 */
public class TestaDesempenho2 {
    public static void main(String[] args) {
        // CRITÉRIO: SET
        // ARRAY LIST VS LINKEDLIST
        // LINKEDLIST WIN

        long inicio,fim;
        double tempo;
        System.out.println("Iniciando com LinkedList...");
        inicio=System.currentTimeMillis();
        List<Integer> lista=new LinkedList<Integer>();
        for(int i=0;i<100000;i++){
            lista.add(0, i);
        }
        fim=System.currentTimeMillis();
        tempo=(fim-inicio)/1000.0;
        System.out.println("Tempo gasto com LinkedList: "+tempo);
        System.out.println("#################################");
        System.out.println("Iniciando com ArrayList...");
        inicio=System.currentTimeMillis();
        lista=new ArrayList<Integer>();
        for(int i=0;i<100000;i++){
            lista.add(0, i);
        }
        fim=System.currentTimeMillis();
        tempo=(fim-inicio)/1000.0;
        System.out.println("Tempo gasto com ArrayList: "+tempo);
    }
}

