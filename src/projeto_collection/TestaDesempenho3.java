package projeto_collection;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by wender on 5/29/16.
 */
public class TestaDesempenho3 {
    public static void main(String[] args) {
        // CRITÉRIO: GET
        // ARRAYLIST VS LINKEDLIST
        // ARRAYLIST WIN

        long inicio,fim;
        double tempo;
        System.out.println("Iniciando com LinkedList...");
        inicio=System.currentTimeMillis();
        List<Integer> lista=new LinkedList<Integer>();
        for(int i=0;i<30000;i++){
            lista.add(i);
        }
        for(int i=0;i<30000;i++){
            lista.get(i);
        }
        fim=System.currentTimeMillis();
        tempo=(fim-inicio)/1000.0;
        System.out.println("Tempo gasto com LinkedList: "+tempo);
        System.out.println("#################################");
        System.out.println("Iniciando com ArrayList...");
        inicio=System.currentTimeMillis();
        lista=new ArrayList<Integer>();
        for(int i=0;i<30000;i++){
            lista.add(i);
        }
        for(int i=0;i<30000;i++){
            lista.get(i);
        }
        fim=System.currentTimeMillis();
        tempo=(fim-inicio)/1000.0;
        System.out.println("Tempo gasto com ArrayList: "+tempo);
    }
}
