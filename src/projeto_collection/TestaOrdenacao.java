package projeto_collection;

import empresa.Conta;

import java.util.*;

/**
 * Created by wender on 5/29/16.
 */
public class TestaOrdenacao {
    public static void main(String[] args) {
//        List <ContaPoupanca> listaContaPoupanca = new ArrayList <ContaPoupanca>();
        List <ContaPoupanca> listaContaPoupanca = new LinkedList <ContaPoupanca>();
        for (int i = 0; i < 20; i++) {
            Conta c = new ContaPoupanca();
            Random r = new Random();
            c.setNumero(r.nextInt()/1000000);
            listaContaPoupanca.add((ContaPoupanca) c);
        }
        for(int i=0; i < listaContaPoupanca.size(); i++){
            Conta atual= listaContaPoupanca.get(i);
            System.out.println("Número: "+atual.getNumero());
        }
        Collections.sort(listaContaPoupanca);
        System.out.println(":::::::::::::::::Lista Ordenada::::::::::::::::::::::::");
        for(int i=0;i<listaContaPoupanca.size();i++){
            Conta atual= listaContaPoupanca.get(i);
            System.out.println("Número: "+atual.getNumero());
        }
        System.out.println(":::::::::::::::::Referencia Lista::::::::::::::::::::::::");
        System.out.println(listaContaPoupanca);
    }
}
