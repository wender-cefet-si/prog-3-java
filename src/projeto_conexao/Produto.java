package projeto_conexao;

/**
 * Created by Wender Machado on 30/05/2016.
 */
public class Produto {
    private int id;
    private String nome;
    private int estoque;

    public Produto(String nome, int estoque){
        this.nome = nome;
        this.estoque = estoque;
    }

    public Produto(int id, String nome, int estoque){
        this.id = id;
        this.nome = nome;
        this.estoque = estoque;
    }

    public int getId() {
        return id;
    }

    public String getNome() {
        return nome;
    }

    public int getEstoque() {
        return estoque;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setEstoque(int estoque) {
        this.estoque = estoque;
    }
}
