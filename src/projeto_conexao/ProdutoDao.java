package projeto_conexao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * Created by Wender Machado on 30/05/2016.
 */
public class ProdutoDao {
    private String sql = null;
    private Connection conexao = null;
    private PreparedStatement stmt = null;

    public ProdutoDao() {
        try {
            this.conexao = new ConnectionFactory().getConexao();
        } catch (RuntimeException e) {
            System.out.println("Erro ao conectar." + e.getMessage());
        }
    }

    /**
     * Função para inserir um produto.
     * @param produto
     */
    public void insert(Produto produto) {
        this.sql = "INSERT INTO produto(nome, estoque) VALUES(?, ?)";
        try {
            this.stmt = this.conexao.prepareStatement(this.sql);
            this.stmt.setString(1, produto.getNome());
            this.stmt.setInt(2, produto.getEstoque());
            this.stmt.execute();
        } catch (SQLException e) {
            System.out.println("Erro ao cadastrar produto "+ produto.getNome());
        }
    }

    /**
     * Função para atualizar atributos de um produto.
     * @param produto
     */
    public void update(Produto produto) {
        this.sql = "UPDATE produto SET nome = ? , estoque = ? WHERE id = ?";
        try {
            this.stmt = this.conexao.prepareStatement(this.sql);
            this.stmt.setString(1, produto.getNome());
            this.stmt.setInt(2, produto.getEstoque());
            this.stmt.setInt(3, produto.getId());
            this.stmt.execute();
        } catch (SQLException e) {
            System.out.println("Erro ao atualizar produto (" + produto.getId() + ") "+ produto.getNome());
        }
    }

    /**
     * Função para deletar produto, recebendo um objeto Produto.
     * @param produto
     */
    public void delete(Produto produto) {
        this.sql = "DELETE produto WHERE id = ?";
        try {
            this.stmt = this.conexao.prepareStatement(this.sql);
            this.stmt.setInt(1, produto.getId());
            this.stmt.execute();
        } catch (SQLException e) {
            System.out.println("Erro ao apagar produto (" + produto.getId() + ") "+ produto.getNome());
        }
    }

    /**
     * Função para deletar produto, recebendo um id do produto.
     * @param id
     */
    public void delete(int id) {
        this.sql = "DELETE produto WHERE id = ?";
        try {
            this.stmt = this.conexao.prepareStatement(this.sql);
            this.stmt.setInt(1, id);
            this.stmt.execute();
        } catch (SQLException e) {
            System.out.println("Erro ao apagar produto com id " + id);
        }
    }
}
