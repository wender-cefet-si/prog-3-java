package projeto_conexao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * Created by Wender Machado on 30/05/2016.
 */
public class TestaInsere {
    public static void main(String[] args) {
        Produto produto = new Produto(1, "Abacaxi", 10);
        ProdutoDao produtoDao = new ProdutoDao();
        produtoDao.insert(produto);
        System.out.println(produto.getNome() + " cadastrado com " + produto.getEstoque());
    }
}
