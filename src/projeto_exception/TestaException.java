package projeto_exception;

/**
 * Created by Wender on 11/04/2016.
 */
public class TestaException {
    public static void main(String[] args) {
        System.out.println("Inicio do main");
        metodo1();
        System.out.println("Fim do main");
    }

    public static void metodo1() {
        System.out.println("Início método 1");
        metodo2();
        System.out.println("Fim método 1");
    }

    public static void metodo2() {
        System.out.println("Início método 2");
        int[] array = new int[5];
        try {
            for (int i = 0; i < 10; i++) {
                array[i] = i;
                System.out.println(array[i]);
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("Limite extrapolado." + e.getMessage());
            e.printStackTrace();
        }
        System.out.println("Fim do método 2");
    }
}
