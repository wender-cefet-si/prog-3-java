package projeto_funcionarios;

/**
 * Created by wender on 4/2/16.
 */
public interface Autenticavel {
    boolean autentica(int senha);
}
