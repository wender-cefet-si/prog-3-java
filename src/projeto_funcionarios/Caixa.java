package projeto_funcionarios;

/**
 * Created by wender on 4/2/16.
 */
public class Caixa extends Funcionario {
    private int numeroDoGuiche;

    @Override
    public double getBonificacao() {
        return this.salario * 0.12;
    }

    public int getNumeroDoGuiche() {
        return numeroDoGuiche;
    }

    public void setNumeroDoGuiche(int numeroDoGuiche) {
        this.numeroDoGuiche = numeroDoGuiche;
    }
}
