package projeto_funcionarios;

/**
 * Created by wender on 4/2/16.
 */
public class ControleDeBonificacoes {
    private double totalDeBonificacoes;

    public void registra(Funcionario f) {
        this.totalDeBonificacoes += f.getBonificacao();
    }

    public double getTotalDeBonificacoes() {
        return totalDeBonificacoes;
    }
}
