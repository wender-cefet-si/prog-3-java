package projeto_funcionarios;

/**
 * Created by wender on 4/2/16.
 */
public class Data {
    private String dia, mes, ano;

    public String getData() {
        return this.dia + "/" + this.mes + "/" + this.ano;
    }

    public String getDia() {
        return dia;
    }

    public void setDia(String dia) {
        this.dia = dia;
    }

    public String getMes() {
        return mes;
    }

    public void setMes(String mes) {
        this.mes = mes;
    }

    public String getAno() {
        return ano;
    }

    public void setAno(String ano) {
        this.ano = ano;
    }
}
