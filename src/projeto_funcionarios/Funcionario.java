package projeto_funcionarios;

/**
 * Created by wender on 4/2/16.
 */
public class Funcionario {
    protected String nome, departamento;
    protected String cpf;
    protected double salario;
    protected boolean ativo;
    protected Data dataDeNascimento = new Data();
    protected static int identificador;

    public Funcionario() {
//..
    }

    public Funcionario(String nome) {
        this.nome = nome;
        identificador++;
    }

    //....
    public double getBonificacao() {
        return this.salario * 0.10;
    }

    public static int getIdentificador() {
        return Funcionario.identificador;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        if (this.valida(cpf))
            this.cpf = cpf;
    }

    private boolean valida(String cpf) {
        return (cpf.length() == 11) ? true : false;
    }

    public void mostra() {
        System.out.println("NOME: " + this.nome);
        System.out.println("SALÁRIO: " + this.salario);
        System.out.println("NASCIMETO: " + this.dataDeNascimento.getData());
        String situacao = (this.ativo) ? "SIM" : "NÃO";
        System.out.println("ESTÁ NA EMPRESA? " + situacao);
    }

    public void aumentarSalario(double percentual) {
        if (percentual > 0)
            this.salario += ((this.salario * percentual) / 100);
    }

    public void demite() {
        this.ativo = false;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public double getSalario() {
        return salario;
    }

    public void setSalario(double salario) {
        this.salario = salario;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public Data getDataDeNascimento() {
        return dataDeNascimento;
    }

    public void setDataDeNascimento(Data dataDeNascimento) {
        this.dataDeNascimento = dataDeNascimento;
    }
}
