package projeto_funcionarios;

/**
 * Created by wender on 4/2/16.
 */
public class Gerente extends Funcionario implements Autenticavel {
    private int senha;
    private int numeroDeFuncionariosGerenciados;

    public Gerente(String nome) {
        super(nome);
    }

    public Gerente() {
    }

    public double getBonificacao() {
        return this.salario * 0.15;
    }

    public boolean autentica(int senha) {
        if (this.senha == senha) {
            System.out.println(this.cpf);
            System.out.println("Acesso Permitido!");
            return true;
        } else {
            System.out.println("Acesso Negado.");
            return false;
        }
    }

    public int getSenha() {
        return senha;
    }

    public void setSenha(int senha) {
        this.senha = senha;
    }

    public int getNumeroDeFuncionariosGerenciados() {
        return numeroDeFuncionariosGerenciados;
    }

    public void setNumeroDeFuncionariosGerenciados(int numeroDeFuncionariosGerenciados) {
        this.numeroDeFuncionariosGerenciados = numeroDeFuncionariosGerenciados;
    }
}
