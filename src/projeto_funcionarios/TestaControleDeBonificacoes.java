package projeto_funcionarios;

/**
 * Created by wender on 4/2/16.
 */
public class TestaControleDeBonificacoes {
    public static void main(String[] args) {
        ControleDeBonificacoes controleDeBonificacoes = new ControleDeBonificacoes();
        Funcionario funcionario = new Funcionario();
        Gerente gerente = new Gerente();

        funcionario.setSalario(2000);
        gerente.setSalario(3000);

        controleDeBonificacoes.registra(funcionario);
        controleDeBonificacoes.registra(gerente);

        System.out.println(controleDeBonificacoes.getTotalDeBonificacoes());
    }
}
