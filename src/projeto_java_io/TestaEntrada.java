package projeto_java_io;

import java.io.*;

/**
 * Created by wender on 5/29/16.
 */
public class TestaEntrada {
    public static void main(String[] args) throws IOException {
        File diretorioArquivoTexto = seNaoExisteDiretorioCria("src/other_files/");
        // Entrada pelo teclado
        // InputStream is = System.in;
        // Entrada por arquivo
        File arquivoEntrada = seNaoExisteArquivoCria(diretorioArquivoTexto, "arquivo.txt");
        InputStream is = new FileInputStream(arquivoEntrada);
        InputStreamReader isr = new InputStreamReader(is);
        BufferedReader br = new BufferedReader(isr);

        File arquivoSaida = seNaoExisteArquivoCria(diretorioArquivoTexto, "saida.txt");

//        OutputStream os = new FileOutputStream(arquivoSaida);
//        OutputStreamWriter osr = new OutputStreamWriter(os);
//        BufferedWriter bw = new BufferedWriter(osr);

        boolean manterInformacoesAnteriores = false;
        FileWriter fw = new FileWriter(arquivoSaida, manterInformacoesAnteriores);
        BufferedWriter bw = new BufferedWriter(fw);

        String linha;
        do {
            linha = br.readLine();
            if (linha != null) {
                System.out.println(linha);
                bw.write(linha);
                bw.newLine();
            }
        } while (linha != null);
        br.close();
        bw.close();
    }

    // Métodos auxiliares

    public static File seNaoExisteDiretorioCria (String urlDiretorio) {
        File diretorio = new File(urlDiretorio);
        if (!diretorio.isDirectory()) {
            diretorio.mkdir();
        }
        return diretorio;
    }

    public static File seNaoExisteDiretorioCria (File diretorio) {
        if (!diretorio.isDirectory()) {
            diretorio.mkdir();
        }
        return diretorio;
    }

    public static File seNaoExisteArquivoCria (String urlDiretorio, String nomeArquivo) throws IOException {
        File arquivo = new File(seNaoExisteDiretorioCria(urlDiretorio) + nomeArquivo);
        if (!arquivo.exists()) {
            arquivo.createNewFile();
        }
        return arquivo;
    }

    public static File seNaoExisteArquivoCria (File diretorio, String nomeArquivo) throws IOException {
        File arquivo = new File(seNaoExisteDiretorioCria(diretorio), nomeArquivo);
        if (!arquivo.exists()) {
            arquivo.createNewFile();
        }
        return arquivo;
    }
}
